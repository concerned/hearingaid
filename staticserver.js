var http = require('http'),
	fs = require('fs'),
	serveStatic  = require('serve-static'),
	finalhandler = require('finalhandler');
var myconf = JSON.parse(fs.readFileSync("./ssconfig.json"));
var serve = serveStatic("./static/");
var httpserver = http.createServer(function(req, res) {
	var done = finalhandler(req, res);
	serve(req, res, done);
});

httpserver.listen(myconf.httpport);
