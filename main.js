var 	fs           = require('fs'),
	ws           = require('nodejs-websocket'),
	irc          = require('irc'),
	strftime     = require('strftime');
var myconf = JSON.parse(fs.readFileSync("./config.json"));
var myBans = {};
var lastMessager = "Nobody";

var ircclient = new irc.Client(myconf.irc_server, myconf.irc_nickname, myconf.irc);
ircclient.addListener('registered', function(msg){
	ircclient.join(myconf.irc_channel_mheard);
	ircclient.join(myconf.irc_channel_support);
});

var wsserver = ws.createServer(function (conn) {
	if (myBans[conn.socket.remoteAddress] != undefined) {
		conn.sendText(JSON.stringify({type: "danger", body:
			"<strong>You're banned!</strong> Reason: "+myBans[conn.socket.remoteAddress]}));
		conn.close();
	}
	conn.on("text", function (str) {
		ircclient.say(myconf.irc_channel_support, "\02Message from "+
			conn.socket.remoteAddress+":\02 "+str);
		lastMessager = conn.socket.remoteAddress;
	});
}).listen(myconf.wsport);

ircclient.addListener('message' + myconf.irc_channel_mheard, function(nick, text, message){
	wsserver.connections.forEach(function(conn){
		conn.sendText(JSON.stringify({type: "info", body: text + "<hr>" + strftime("%r %D %Z %z")}));
	});
});

var supportOps = {};
ircclient.addListener('names' + myconf.irc_channel_support, function(nicks){
	supportOps = nicks;
});

ircclient.addListener('message' + myconf.irc_channel_support, function(nick, text, message){
	var comm = text.split(" ");
	if (comm[0] == "!t") {
		comm.shift();
		var i = 0;
		var wantedip = comm.shift();
		wsserver.connections.forEach(function(conn){
			if (conn.socket.remoteAddress == wantedip) {
				conn.sendText(JSON.stringify({type: "success", body:
					"<strong>Message from "+nick+"</strong>: "+ comm.join(" ") +
					"<hr>" + strftime("%r %D %Z %z")}));
				i = i + 1;
			}
		});
		ircclient.say(myconf.irc_channel_support, "Sent message to \02"+i+" clients\02 connected from that address.");
	} else if (comm[0] == "!r") {
		comm.shift();
		var i = 0;
		var wantedip = lastMessager;
		wsserver.connections.forEach(function(conn){
			if (conn.socket.remoteAddress == wantedip) {
				conn.sendText(JSON.stringify({type: "success", body:
					"<strong>Message from "+nick+"</strong>: "+ comm.join(" ") +
					"<hr>" + strftime("%r %D %Z %z")}));
				i = i + 1;
			}
		});
		ircclient.say(myconf.irc_channel_support, "Sent message to \02"+i+" clients\02 connected from \02" + wantedip +"\02.");
	} else if (comm[0] == "!broadcast") {
		comm.shift();
		if (supportOps[nick] == "@") {
			var i = 0;
			wsserver.connections.forEach(function(conn){
				conn.sendText(JSON.stringify({type: "success", body:
					"<strong>Message to all clients from "+nick+"</strong>: "+ comm.join(" ") +
					"<hr>" + strftime("%r %D %Z %z")}));
					i = i + 1;
			});
			ircclient.say(myconf.irc_channel_support, "Sent message to \02"+i+" clients\02.");
		} else {
			ircclient.say(myconf.irc_channel_support, "\02You're not an operator!\02");
		}
	} else if (comm[0] == "!dc") {
		comm.shift();
		if (supportOps[nick] == "@") {
			var i = 0;
			var wantedip = comm.shift();
			wsserver.connections.forEach(function(conn){
				if (conn.socket.remoteAddress == wantedip) {
					conn.sendText(JSON.stringify({type: "danger", body:
						"<strong>You were disconnected by an operator!</strong> " +
						comm.join(" ") + "<hr>" + strftime("%r %D %Z %z")}));
					conn.close();
					i = i + 1;
				}
			})
			ircclient.say(myconf.irc_channel_support, "Disconnected \02"+i+" clients\02 connected from that address.");
		} else {
			ircclient.say(myconf.irc_channel_support, "\02You're not an operator!\02");
		}
	} else if (comm[0] == "!block") {
		comm.shift();
		if (supportOps[nick] == "@") {
			myBans[comm.shift()] = comm.join(" ");
			ircclient.say(myconf.irc_channel_support, "\02Current block list:\02 " + JSON.stringify(myBans));
		} else {
			ircclient.say(myconf.irc_channel_support, "\02You're not an operator!\02");
		}
	} else if (comm[0] == "!clearblocks") {
		if (supportOps[nick] == "@") {
			ircclient.say(myconf.irc_channel_support, "\02Current bans list:\02 " + JSON.stringify(myBans));
			myBans = {};
			ircclient.say(myconf.irc_channel_support, "\02Current block list:\02 " + JSON.stringify(myBans));
		} else {
			ircclient.say(myconf.irc_channel_support, "\02You're not an operator!\02");
		}
	} else if (comm[0] == "!blocklist") {
		ircclient.say(myconf.irc_channel_support, "\02Current bans list:\02 " + JSON.stringify(myBans));
	} else if (comm[0] == "!clients") {
		var i = 0;
		var clist = [];
		wsserver.connections.forEach(function(conn){
			i = i + 1;
			clist.push(conn.socket.remoteAddress);
		});
		ircclient.say(myconf.irc_channel_support, "There are \02"+i+" clients\02 connected in total. "+JSON.stringify(clist));
	} else if (comm[0] == "!dcall") {
		comm.shift();
		if (supportOps[nick] == "@") {
			wsserver.connections.forEach(function(conn){
				conn.sendText(JSON.stringify({type: "danger", body:
					"<strong>All clients were disconnected.</strong> " +
					comm.join(" ")}));
				conn.close();
			})
		} else {
			ircclient.say(myconf.irc_channel_support, "\02You're not an operator!\02");
		}
	} else if (comm[0] == "!opsync") {
			ircclient.send("NAMES", myconf.irc_channel_support);
			ircclient.say(myconf.irc_channel_support, "\02Updated list of operators in channel.");
	} else if (comm[0] == "!help") {
		ircclient.say(myconf.irc_channel_support, "\02Commands:\02 !t [ip] [message], !r, !blocklist, !clients, !opsync");
		ircclient.say(myconf.irc_channel_support, "\02Operator commands:\02 !dc [ip] [reason], !block [ip] [reason], !clearblocks, !dcall [reason], !broadcast [msg]");
	}
});

ircclient.addListener('+mode', function(channel, by, mode, argument, message){
	if (channel == myconf.irc_channel_support) {
		if (mode == "o") {
			ircclient.send("NAMES", myconf.irc_channel_support);
			ircclient.say(myconf.irc_channel_support, "\02Updated list of operators in channel.");
		}
	}
});

ircclient.addListener('-mode', function(channel, by, mode, argument, message){
	if (channel == myconf.irc_channel_support) {
		if (mode == "o") {
			ircclient.send("NAMES", myconf.irc_channel_support);
			ircclient.say(myconf.irc_channel_support, "\02Updated list of operators in channel.");
		}
	}
});
