var connectTo = "ws://"+window.location.hostname+":8081/";
var mySocket = new WebSocket(connectTo);
var ding = new Audio("./ding.wav");
var focused = true;
var unreadMsgs = 0;
var favicon = new Favico({animation: 'popFade'});
function clearMyStuff(){
	$("#myStuffHolder").html("");
}
function putData(txt) {
	$("#myStuffHolder").append(txt);
	$('html, body').animate({ scrollTop: $(document).height() }, 1200);
	if (focused == false) {
		unreadMsgs = unreadMsgs + 1;
		favicon.badge(unreadMsgs);
	}
}
mySocket.onmessage = function(event) {
	mydata = JSON.parse(event.data);
	if (mydata.type == "info") {
		putData('<div class="alert alert-info" role="alert">' +
			mydata.body + '</div>');
		if($("#allnotify").is(':checked')){
			ding.play();
		}
	} else if (mydata.type == "success") {
		putData('<div class="alert alert-success" role="alert">' +
			mydata.body + '</div>');
		ding.play();
	} else if (mydata.type == "danger") {
		putData('<div class="alert alert-danger" role="alert">' +
			mydata.body + '</div>');
	}
}
mySocket.onclose = function(event) {
	putData('<div class="alert alert-danger" role="alert">' +
	'<strong>Socket connection closed!</strong> Code '+ event.code + '</div>');
}
function sendToOpers() {
	if(mySocket.readyState == mySocket.OPEN) {
		var ogmsg = $("#opmsg").val();
		if (ogmsg != "") {
			mySocket.send(ogmsg);
			putData('<div class="alert alert-warning" role="alert">'+
				'<strong>You submitted a message to the operators:</strong><br/>'+ogmsg+'</div>');
			$("#opmsg").val("");
		}
	}
}
$("#sendopmsg").click(function() {
	sendToOpers();
});
$("#opmsg").keyup(function (e) {
	if (e.keyCode == 13) {
		sendToOpers();
	}
});
$(window).blur(function(){focused = false;});
$(window).focus(function(){focused = true; favicon.reset(); unreadMsgs = 0;});
