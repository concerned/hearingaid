# HearingAid

## Features

  - Shows data from a given data IRC channel
  - Allows direct, moderated user interaction on a support IRC channel
  - Uses new, bandwidth-saving, easy web technologies
  - Compatible with any screen size (which means embeddable in a sidebar, and mobile-friendly)
  - Multiple notification methods, like favicon badges and sounds

## Notes

  - You can put the static directory wherever you want on a different webserver.
  - The `staticserver.js` file is just a quick webserver that you shouldn't use.
  - Change the first line of `static/client.js` to strictly the location of the websocket server.
  - **Be sure to see `config.json`!**
  - After configuration, just use `node server.js`.
  - Be sure to run with the latest Node, not 0.10.x/etc.

Originally made for the [QuadNet Open ircDDB gateway](http://openquad.net).

## Commands
Commands are done within the support IRC channel.

### Any user commands
| ! | Args | Meaning
|---:|:---:|:---
| `!t` | `[ip] [message...]` | send a message to clients on an IP
| `!r` | `[message...]` | quick reply to the last IP to post
| `!blocklist` | | view blocks/bans
| `!clients` | | view list of connected clients
| `!opsync` | | sync names, acknowledge new operators

### Operator-only commands
| ! | Args | Meaning
|---:|:---:|:---
| `!dc` | `[ip] [reason...]` | disconnect clients on an IP
| `!block` | `[ip] [reason...]` | auto-disconnect clients on this IP
| `!clearblocks` | | remove all entries from blocklist
| `!dcall` | `[reason...]` | disconnect all clients
| `!broadcast` | `[message...]` | send a message to all clients
